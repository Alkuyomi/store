import 'package:shared_preferences/shared_preferences.dart';

String dataCat = "ALL" ;

setEmail(String email) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('email', email);
}

 getEmail() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('email');
  return email ;
}

setName(String name) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('name', name);
}

getName() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('name');
  return email ;
}

setPhone(String name) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('phone', name);
}

getPhone() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('phone');
  return email ;
}

setLocation(String location) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('location', location);
}

getLocation() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('location');
  return email ;
}

