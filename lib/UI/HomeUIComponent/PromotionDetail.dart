
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:store_main_version/ListItem/ProductData.dart';
import 'package:store_main_version/ListItem/Data.dart';
import 'package:store_main_version/UI/HomeUIComponent/DetailProduct.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class promoDetail extends StatefulWidget {
  @override
  _promoDetailState createState() => _promoDetailState();
}

class _promoDetailState extends State<promoDetail> {
  final String url = "https://us-central1-grocery-project-1a3b7.cloudfunctions.net/getProducts";
  List data;

// Function to get the JSON data
  Future<String> getJSONData() async {
    var response = await http.get(
      // Encode the url
        Uri.encodeFull(url),
        // Only accept JSON response
        headers: {"Accept": "application/json"});

    // Logs the response body to the console
    //print(response.body);

    setState(() {
      // Get the JSON data
      var dataConvertedToJSON = json.decode(response.body);
      // Extract the required part and assign it to the global variable named data
      data = dataConvertedToJSON['result'];

      data.forEach((f) => {
        dataCat == "ALL" ? productItems.add(
          productData(
          barcode: f['barcode'] ,
          category: f['category'] ,
          code: f['code'] ,
          english_name: f['english_name'] ,
          arabic_name: f['arabic_name'] ,
          packing: f['packing'] ,
          price: f['price'] ,
          size: f['size'] ,
          inCart: false ,
          unit: f['unit']
      )
      ) : dataCat == f['category'] ? productItems.add(
      productData(
      barcode: f['barcode'] ,
      category: f['category'] ,
      code: f['code'] ,
      english_name: f['english_name'] ,
      arabic_name: f['arabic_name'] ,
      packing: f['packing'] ,
      price: f['price'] ,
      size: f['size'] ,
      inCart: false ,
      unit: f['unit']
      )
      ) : null

      });
      fullProductItems = productItems ;


    });
    return "Successfull";
  }

  ///
  /// Get image data dummy from firebase server
  ///
  var imageNetwork = NetworkImage("https://firebasestorage.googleapis.com/v0/b/beauty-look.appspot.com/o/Screenshot_20181005-213931.png?alt=media&token=e6287f67-5bc0-4225-8e96-1623dc9dc42f");

  ///
  /// check the condition is right or wrong for image loaded or no
  ///
  bool imageLoad = true;

  ///
  /// SetState after imageNetwork loaded to change list card
  ///
  @override
  void initState() {
    getJSONData();
    imageNetwork.resolve(new ImageConfiguration()).addListener((_,__){
      if(mounted){
        setState(() {
          imageLoad = false;
        });
      }
    });
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {


    /// Grid Item a product
    var _grid = SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ///
            ///
            /// check the condition if image data from server firebase loaded or no
            /// if image true (image still downloading from server)
            /// Card to set card loading animation
            ///
            ///
            imageLoad? _imageLoading(context):
            GridView.count(
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 10.0),
              crossAxisSpacing: 10.0,
              mainAxisSpacing: 15.0,
              childAspectRatio: 0.670,
              crossAxisCount: 2,
              primary: false,
              children:List.generate(
                /// Get data in flashSaleItem.dart (ListItem folder)
                productItems.length,
                    (index) => ItemGrid(productItems[index]),
              ),
            )
          ],
        ),
      ),
    );

    return Scaffold(
      /// Appbar item
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: TextField(
          onChanged: (text){
            setState(() {
              productItems = fullProductItems ;
              productItems = productItems.where((p) => p.arabic_name.toUpperCase().contains(text.toUpperCase())).toList();
            });
          },
          decoration: InputDecoration(
              border: InputBorder.none,
              icon: Icon(
                Icons.search,
                color: Colors.black38,
                size: 18.0,
              ),
              hintText: 'What are you looking for?'
          ),
        ),
        iconTheme: IconThemeData(
          color: Color(0xFFf73f52),
        ),
        elevation: 0.0,
      ),
      body:  _grid,
    );
  }
}

/// ItemGrid class
class ItemGrid extends StatelessWidget {
  @override
  productData item ;
  ItemGrid(this.item);

  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);

    return InkWell(
      child: Container(

        decoration: BoxDecoration(

            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF656565).withOpacity(0.15),
                blurRadius: 2.0,
                spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
              )
            ]),
        child: Wrap(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(

                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: mediaQueryData.size.height / 20.3),
                      height: mediaQueryData.size.height / 3.3,
                      width: 200.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)),

                      ),
                      child: Image.network('https://res.cloudinary.com/doxsoucmq/image/upload/v1539281234/images/${item.barcode}F.jpg'),
                    ),

                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 7.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    item.arabic_name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 1.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    item.price + " OMR",
                    style: TextStyle(
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0),
                  ),
                ),


              ],
            ),
          ],
        ),
      ),
      onTap: () {
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => new detailProduk(item),
            transitionDuration: Duration(milliseconds: 900),

            /// Set animation Opacity in route to detailProduk layout
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            }));
      },
    );

  }
}

///
///
///
/// Loading Item Card Animation Constructor
///
///
///
class loadingMenuItemCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: 20.0, left: 10.0, bottom: 10.0, right: 0.0),
      child: InkWell(
        onTap: (){

        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF656565).withOpacity(0.15),
                  blurRadius: 2.0,
                  spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
                )
              ]),
          child: Wrap(
            children: <Widget>[
              ///
              ///
              /// Shimmer class for animation loading
              ///
              ///
              Shimmer.fromColors(
                baseColor: Colors.black38,
                highlightColor: Colors.white,
                child: Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            height: 205.0,
                            width: 185.0,
                            color: Colors.black12,
                          ),
                          Container(
                            height: 25.5,
                            width: 65.0,
                            decoration: BoxDecoration(
                                color: Color(0xFFD7124A),
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(20.0),
                                    topLeft: Radius.circular(5.0))),
                          )
                        ],
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 5.0,top: 12.0),
                          child: Container(
                            height: 9.5,
                            width: 130.0,
                            color: Colors.black12,
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 5.0,top: 10.0),
                          child: Container(
                            height: 9.5,
                            width: 80.0,
                            color: Colors.black12,
                          )
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 15.0, right: 15.0, top: 7.0,bottom: 0.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.star,
                                  color: Colors.yellow,
                                  size: 14.0,
                                )
                              ],
                            ),
                            Container(
                              height: 8.0,
                              width: 30.0,
                              color: Colors.black12,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


///
///
/// Calling imageLoading animation for set a grid layout
///
///
Widget _imageLoading(BuildContext context){
  return GridView.count(
    shrinkWrap: true,
    padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 10.0),
    crossAxisSpacing: 10.0,
    mainAxisSpacing: 15.0,
    childAspectRatio: 0.545,
    crossAxisCount: 2,
    primary: false,
    children:List.generate(
      /// Get data in PromotionDetail.dart (ListItem folder)
      productItems.length,
          (index) => loadingMenuItemCard(),
    ),
  );
}
