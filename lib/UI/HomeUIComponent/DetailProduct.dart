import 'package:store_main_version/Library/carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:store_main_version/ListItem/CartItemData.dart';
import 'package:store_main_version/ListItem/HomeGridItemRecomended.dart';
import 'package:store_main_version/UI/CartUIComponent/CartLayout.dart';
import 'package:store_main_version/UI/HomeUIComponent/ChatItem.dart';
import 'package:store_main_version/UI/CartUIComponent/Delivery.dart';
import 'package:store_main_version/ListItem/ProductData.dart';


import 'package:flutter_rating/flutter_rating.dart';
import 'package:store_main_version/UI/HomeUIComponent/ReviewLayout.dart';

class detailProduk extends StatefulWidget {
  productData gridItem;

  detailProduk(this.gridItem);

  @override
  _detailProdukState createState() => _detailProdukState(gridItem);
}

/// Detail Product for Recomended Grid in home screen
class _detailProdukState extends State<detailProduk> {
  double rating = 3.5;
  int starCount = 5;
  /// Declaration List item HomeGridItemRe....dart Class
  final productData gridItem;
  _detailProdukState(this.gridItem);

  @override
  static BuildContext ctx;
  int valueItemChart = cartItems.length;
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();



  /// Custom Text black
  static var _customTextStyle = TextStyle(
    color: Colors.black,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var _subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Custom Text for Detail title
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);



  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        actions: <Widget>[
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                  PageRouteBuilder(pageBuilder: (_, __, ___) => new cart()));
            },
            child: Stack(
              alignment: AlignmentDirectional(-1.0, -0.8),
              children: <Widget>[
                IconButton(
                  onPressed: null,
                    icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.black26,
                )),
                CircleAvatar(
                  radius: 10.0,
                  backgroundColor: Colors.red,
                  child: Text(
                    valueItemChart.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 13.0),
                  ),
                ),
              ],
            ),
          ),
        ],
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Product Details",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            color: Colors.black54,
            fontSize: 17.0,
            fontFamily: "Gotik",
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  /// Header image slider
                  Container(

                    height: 300.0,
                    child: Hero(
                      tag: "hero-grid-${gridItem.barcode}",
                      child: Material(
                        child: new Carousel(
                          overlayShadowColors: Colors.white,
                          dotColor: Colors.black26,
                          dotIncreaseSize: 1.7,
                          dotBgColor: Colors.transparent,
                          autoplay: false,
                          boxFit: BoxFit.cover,
                          images: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white
                              ),
                              child:Image.network('https://res.cloudinary.com/doxsoucmq/image/upload/v1539281234/images/${gridItem.barcode}F.jpg'),

                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white
                              ),
                              child:Image.network('https://res.cloudinary.com/doxsoucmq/image/upload/v1539281234/images/${gridItem.barcode}B.jpg'),

                            ),

                          ],
                        ),
                      ),
                    ),
                  ),
                  /// Background white title,price and ratting
                  Container(
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                        color: Color(0xFF656565).withOpacity(0.15),
                        blurRadius: 1.0,
                        spreadRadius: 0.2,
                      )
                    ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            gridItem.arabic_name,
                            style: _customTextStyle,
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          Text(
                            gridItem.price,
                            style: _customTextStyle,
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.0),
                            child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     
                          ),
                          ),
                        ],
                      ),
                    ),
                  ),


                  /// Background white for description
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Container(
                      height: 205.0,
                      width: 600.0,
                      decoration:
                          BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                          color: Color(0xFF656565).withOpacity(0.15),
                          blurRadius: 1.0,
                          spreadRadius: 0.2,
                        )
                      ]),
                      child: Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "Specifications",
                                style: _subHeaderCustomStyle,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0,
                                  right: 20.0,
                                  bottom: 10.0,
                                  left: 20.0),
                              child: Container(

                                child: new Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(

                                            child: new Text("Packing"),
                                          ),
                                          new Container(
                                            child: new Text("Size"),
                                          ),
                                          new Container(
                                            child: new Text("Unit"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            child: new Text(gridItem.packing),
                                          ),
                                          new Container(
                                            child: new Text(gridItem.size),
                                          ),
                                          new Container(
                                            child: new Text(gridItem.unit),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),


                ],
              ),
            ),
          ),

          /// If user click icon chart SnackBar show
          /// this code to show a SnackBar
          /// and Increase a valueItemChart + 1
//          InkWell(
//            onTap: () {
//              cartItems.add(cartItem(id: int.parse(gridItem.barcode) ,img: "https://res.cloudinary.com/doxsoucmq/image/upload/v1539281234/images/${gridItem.barcode}F.jpg" ,name: gridItem.name ,price: double.parse(gridItem.price) ,quantity: 1 ));
//              var snackbar = SnackBar(
//                content: Text(gridItem.name + " added to cart"),
//              );
//              setState(() {
//                valueItemChart++;
//              });
//              _key.currentState.showSnackBar(snackbar);
//            },
//            child: Padding(
//              padding: const EdgeInsets.only(bottom: 5.0),
//              child: Container(
//                color: Colors.white,
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    Container(
//                      height: 40.0,
//                      width: 60.0,
//                      decoration: BoxDecoration(
//                          color: Colors.white12.withOpacity(0.1),
//                          border: Border.all(color: Colors.black12)),
//                      child: Center(
//                        child: Image.asset(
//                          "assets/icon/shopping-cart.png",
//                          height: 23.0,
//                        ),
//                      ),
//                    ),
//
//
//
//                    /// Button Pay
////                    InkWell(
////                      onTap: () {
////                        Navigator.of(context).push(PageRouteBuilder(
////                            pageBuilder: (_, __, ___) => new delivery()));
////                      },
////                      child: Container(
////                        height: 45.0,
////                        width: 200.0,
////                        decoration: BoxDecoration(
////                          color: Colors.indigoAccent,
////                        ),
////                        child: Center(
////                          child: Text(
////                            "Pay",
////                            style: TextStyle(
////                                color: Colors.white,
////                                fontWeight: FontWeight.w700),
////                          ),
////                        ),
////                      ),
////                    ),
//                  ],
//                ),
//              ),
//            ),
//          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(onPressed: () {
        if(!gridItem.inCart ){
          cartItems.add(cartItem(id: int.parse(gridItem.barcode) ,img: "https://res.cloudinary.com/doxsoucmq/image/upload/v1539281234/images/${gridItem.barcode}F.jpg" ,name: gridItem.arabic_name ,price: double.parse(gridItem.price) ,quantity: 1 ));
          var snackbar = SnackBar(
            content: Text(gridItem.arabic_name + " added to cart"),
          );
          setState(() {
            gridItem.inCart = true ;

            valueItemChart++;
          });
          _key.currentState.showSnackBar(snackbar);
        }else{
          setState(() {
            cartItems = cartItems.where((p) => p.id.toString() != gridItem.barcode.toString() ).toList();
            gridItem.inCart = false ;
            valueItemChart--;
          });

        }

      },
      label:  Text(gridItem.inCart == false ? "Add to cart" : "Remove from cart")  ,
      icon: Icon(gridItem.inCart == false ? Icons.shopping_cart : Icons.remove_shopping_cart),
      backgroundColor: gridItem.inCart == false ? Colors.blue : Colors.red,),

    );
  }


  Widget _buildRating(String date, String details, Function changeRating,String image) {
    return ListTile(
      leading: Container(
        height: 45.0,
        width: 45.0,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage(image),fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(50.0))
        ),
      ),
      title: Row(
        children: <Widget>[
          StarRating(
              size: 20.0,
              rating: 3.5,
              starCount: 5,
              color: Colors.yellow,
              onRatingChanged: changeRating),
          SizedBox(width: 8.0),
          Text(
            date,
            style: TextStyle(fontSize: 12.0),
          )
        ],
      ),
      subtitle: Text(details,style: _detailText,),
    );
  }
}



/// RadioButton for item choose in size
class RadioButtonCustom extends StatefulWidget {
  String txt;

  RadioButtonCustom({this.txt});

  @override
  _RadioButtonCustomState createState() => _RadioButtonCustomState(this.txt);
}

class _RadioButtonCustomState extends State<RadioButtonCustom> {
  _RadioButtonCustomState(this.txt);

  String txt;
  bool itemSelected = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
        setState(() {
              if (itemSelected == false) {
                setState(() {
                  itemSelected = true;
                });
              } else if (itemSelected == true) {
                setState(() {
                  itemSelected = false;
                });
              }
            });
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
              shape: BoxShape.circle),
          child: Center(
            child: Text(
              txt,
              style: TextStyle(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
            ),
          ),
        ),
      ),
    );
  }
}

/// RadioButton for item choose in color
class RadioButtonColor extends StatefulWidget {
  Color clr;

  RadioButtonColor(this.clr);

  @override
  _RadioButtonColorState createState() => _RadioButtonColorState(this.clr);
}

class _RadioButtonColorState extends State<RadioButtonColor> {
  bool itemSelected = true;
  Color clr;

  _RadioButtonColorState(this.clr);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          if (itemSelected == false) {
            setState(() {
              itemSelected = true;
            });
          } else if (itemSelected == true) {
            setState(() {

              itemSelected = false;
            });
          }
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: clr,
              border: Border.all(
                  color: itemSelected ? Colors.black26 : Colors.indigoAccent,
                  width: 2.0),
              shape: BoxShape.circle),
        ),
      ),
    );
  }
}

/// Class for card product in "Top Rated Products"
class FavoriteItem extends StatelessWidget {
  String image, Rating, Salary, title, sale;

  FavoriteItem({this.image, this.Rating, this.Salary, this.title, this.sale});

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Padding(
      padding: const EdgeInsets.only(left: 5.0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF656565).withOpacity(0.15),
                blurRadius: 4.0,
                spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
              )
            ]),
        child: Wrap(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 150.0,
                  width: 150.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(7.0),
                          topRight: Radius.circular(7.0)),
                      image: DecorationImage(
                          image: AssetImage(image), fit: BoxFit.cover)),
                ),
                Padding(padding: EdgeInsets.only(top: 15.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    title,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 1.0)),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Text(
                    Salary,
                    style: TextStyle(
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 14.0),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            Rating,
                            style: TextStyle(
                                fontFamily: "Sans",
                                color: Colors.black26,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0),
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.yellow,
                            size: 14.0,
                          )
                        ],
                      ),
                      Text(
                        sale,
                        style: TextStyle(
                            fontFamily: "Sans",
                            color: Colors.black26,
                            fontWeight: FontWeight.w500,
                            fontSize: 12.0),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget _line(){
  return  Container(
    height: 0.9,
    width: double.infinity,
    color: Colors.black12,
  );
}
