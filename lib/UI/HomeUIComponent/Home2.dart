import 'package:store_main_version/Library/carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:store_main_version/ListItem/HomeGridItemRecomended.dart';
import 'package:store_main_version/UI/HomeUIComponent/AppbarGradient.dart';
import 'package:store_main_version/Library/countdown/countdown.dart';
import 'package:store_main_version/UI/HomeUIComponent/CategoryDetail.dart';
import 'package:store_main_version/UI/HomeUIComponent/DetailProduct.dart';
import 'package:store_main_version/UI/HomeUIComponent/PromotionDetail.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

/// Component all widget in home
class _MenuState extends State<Menu> with TickerProviderStateMixin {
  /// Declare class GridItem from HomeGridItemReoomended.dart in folder ListItem
  GridItem gridItem;

  bool isStarted = false;
  var hourssub, minutesub, secondsub;
  /// CountDown for timer
  CountDown hours, minutes, seconds;
  int hourstime, minute, second = 0;

  /// Set for StartStopPress CountDown
  onStartStopPress() {
    if (this.secondsub == null) {
      secondsub = seconds.stream.listen(null);
      secondsub.onData((Duration d) {
        setState(() {
          second = d.inSeconds;
        });
      });
    }
    if (this.minutesub == null) {
      minutesub = minutes.stream.listen(null);
      minutesub.onData((Duration d) {
        setState(() {
          minute = d.inMinutes;
        });
      });
    }
    if (this.hourssub == null) {
      hourssub = hours.stream.listen(null);
      hourssub.onData((Duration d) {
        setState(() {
          hourstime = d.inHours;
        });
      });
    }
  }

  /// To set duration initState auto start if FlashSale Layout open
  @override
  void initState() {
    hours = new CountDown(new Duration(hours: 24));
    minutes = new CountDown(new Duration(hours: 1));
    seconds = new CountDown(new Duration(minutes: 1));

    onStartStopPress();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    MediaQueryData mediaQueryData = MediaQuery.of(context);
    double size = mediaQueryData.size.height;

    /// Navigation to MenuDetail.dart if user Click icon in category Menu like a example camera
    var onClickMenuIcon = () {
      Navigator.of(context).push(PageRouteBuilder(
          pageBuilder: (_, __, ___) => new promoDetail(),
          transitionDuration: Duration(milliseconds: 750),
          /// Set animation with opacity
          transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return Opacity(
              opacity: animation.value,
              child: child,
            );
          }));
    };


    /// Declare device Size
    var deviceSize = MediaQuery.of(context).size;

    /// ImageSlider in header
    var imageSlider = Container(
      height: 182.0,
      child: new Carousel(
        boxFit: BoxFit.cover,
        dotColor: Color(0xFFf73f52).withOpacity(0.8),
        dotSize: 5.5,
        dotSpacing: 16.0,
        dotBgColor: Colors.transparent,
        showIndicator: true,
        overlayShadow: true,
        overlayShadowColors: Colors.white.withOpacity(0.9),
        overlayShadowSize: 0.9,
        images: [
          Image.network("https://res.cloudinary.com/doxsoucmq/image/upload/v1557351285/banner/banner1.png"),
          Image.network("https://res.cloudinary.com/doxsoucmq/image/upload/v1557351285/banner/banner2.png"),
          Image.network("https://res.cloudinary.com/doxsoucmq/image/upload/v1557351285/banner/banner3.png"),
          Image.network("https://res.cloudinary.com/doxsoucmq/image/upload/v1557351285/banner/banner4.png"),
          Image.network("https://res.cloudinary.com/doxsoucmq/image/upload/v1557351285/banner/banner5.png"),
        ],
      ),
    );

    /// CategoryIcon Component
    var categoryIcon = Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 20.0),
      alignment: AlignmentDirectional.centerStart,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Padding(padding: EdgeInsets.only(top: 20.0)),

          /// Get class CategoryIconValue
          CategoryIconValue(
            tap1: onClickMenuIcon,
            icon1: "assets/icons/beverages.png",
            title1: "Beverages",
            tap2: onClickMenuIcon,
            icon2: "assets/icons/grains.png",
            title2: "Spices & Grains",
          ),
          Padding(padding: EdgeInsets.only(top: 23.0)),
          CategoryIconValue(
            tap1: onClickMenuIcon,
            icon1: "assets/icons/milk.png",
            title1: "Dairy Products",
            tap2: onClickMenuIcon,
            icon2: "assets/icons/can.png",
            title2: "Oil & Canning",
          ),
          Padding(padding: EdgeInsets.only(top: 23.0)),
          CategoryIconValue(
            icon1: "assets/icons/snacks.png",
            tap1: onClickMenuIcon,
            title1: "Desserts & Snacks ",
            icon2: "assets/icons/lotion.png",
            tap2: onClickMenuIcon,
            title2: "Personal Care",
          ),
          Padding(padding: EdgeInsets.only(top: 23.0)),
          CategoryIconValue(
            icon1: "assets/icons/cleaner.png",
            tap1: onClickMenuIcon,
            title1: "Cleaners",
            icon2: "assets/icons/baby.png",
            tap2: onClickMenuIcon,
            title2: "Baby Care ",
          ),
          Padding(padding: EdgeInsets.only(top: 23.0)),
          CategoryIconValue(
            icon1: "assets/icons/kit.png",
            tap1: onClickMenuIcon,
            title1: "Kitchen Goods",
            icon2: "assets/icons/bread.png",
            tap2: onClickMenuIcon,
            title2: "Bakery &\n Baking Goods",

          ),

          Padding(padding: EdgeInsets.only(bottom: 30.0))
        ],
      ),
    );



    var search = Container(
      margin: EdgeInsets.only(left: mediaQueryData.padding.left + 15),
      height: 37.0,
      width: 222.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          shape: BoxShape.rectangle,
          gradient: LinearGradient(
              colors: [
                const Color(0xFFA3BDED),
                const Color(0xFFf73f52),
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp)),
      child: Row(

        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[

          Padding(padding: EdgeInsets.only(left: 17.0)),
          Image.asset(
            "assets/img/search2.png",
            height: 22.0,
          ),
          Padding(
              padding: EdgeInsets.only(
                left: 17.0,
              )),
          Padding(
            padding: EdgeInsets.only(top: 3.0),
            child: Text(
              "Search",
              style: TextStyle(

                  fontFamily: "Popins",
                  color: Colors.black12,
                  fontWeight: FontWeight.w900,
                  letterSpacing: 0.0,
                  fontSize: 16.4),
            ),
          ),
        ],
      ),
    );




    return Scaffold(
      backgroundColor: Colors.white,

      /// Use Stack to costume a appbar
      body:  Stack(

        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[


                /// Call var imageSlider
//                  imageSlider,

                Padding(
                    padding: EdgeInsets.only(
                        top: mediaQueryData.padding.top + 10.5)),
//                Padding(
//                  padding: EdgeInsets.only(bottom: mediaQueryData.padding.bottom + 10.5),
//                  child: FloatingActionButton.extended(onPressed: null,
//                    label: Text("Search                      "),
//                    icon: Icon(Icons.search),
//                    backgroundColor: Colors.white,
//                    foregroundColor: Colors.black12,
//
//
//                  ) ,
//                )


                /// Call var categoryIcon
                categoryIcon,


              ],
            ),
          ),

          /// Get a class AppbarGradient
          /// This is a Appbar in home activity

        ],
      ),


    );
  }
}





/// Component item Menu icon bellow a ImageSlider
class CategoryIconValue extends StatelessWidget {
  String icon1, icon2, title1, title2;
  GestureTapCallback tap1, tap2;

  CategoryIconValue(
      {this.icon1,
        this.tap1,
        this.icon2,
        this.tap2,
        this.title1,
        this.title2
      });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          onTap: tap1,
          child: Column(
            children: <Widget>[
              Image.asset(
                icon1,
                height: 30.2,
              ),
              Padding(padding: EdgeInsets.only(top: 7.0)),
              Text(
                title1,
                style: TextStyle(
                  fontFamily: "Sans",
                  fontSize: 10.0,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ),
        InkWell(
          onTap: tap2,
          child:  Column(
            children: <Widget>[
              Image.asset(
                icon2,
                height: 30.2,
              ),
              Padding(padding: EdgeInsets.only(top: 7.0)),
              Text(
                title2,
                style: TextStyle(
                  fontFamily: "Sans",
                  fontSize: 10.0,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ),

      ],
    );
  }
}
