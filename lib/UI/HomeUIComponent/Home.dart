import 'package:store_main_version/Library/carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:store_main_version/ListItem/HomeGridItemRecomended.dart';
import 'package:store_main_version/ListItem/Data.dart';
import 'package:store_main_version/UI/HomeUIComponent/AppbarGradient.dart';
import 'package:store_main_version/Library/countdown/countdown.dart';
import 'package:store_main_version/UI/HomeUIComponent/CategoryDetail.dart';
import 'package:store_main_version/UI/HomeUIComponent/DetailProduct.dart';
import 'package:store_main_version/UI/HomeUIComponent/PromotionDetail.dart';
import 'package:store_main_version/UI/HomeUIComponent/modal/category.dart';



class Menu extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
          primaryColor: Color.fromRGBO(58, 66, 86, 1.0), fontFamily: 'Raleway'),
      home: new ListPage(title: 'Categories'),
      // home: DetailPage(),
    );
  }
}

// LIST 



class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List Categorys;

  @override
  void initState() {
    Categorys = getCategorys();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(Category Category) => ListTile(
      contentPadding:
      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      leading: Container(
        padding: EdgeInsets.only(right: 12.0),
        decoration: new BoxDecoration(

            border: new Border(
                right: new BorderSide(width: 1.0, color: Colors.white24))
        ),
        child: Image.asset(Category.content , height: 40.0 ,),
      ),
      title: Text(
        Category.title,
        style: TextStyle(color: Colors.black45 , fontWeight: FontWeight.bold),
      ),
      // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),


      trailing:
      Icon(Icons.keyboard_arrow_right, color: Color(0xFFf73f52), size: 30.0),
      onTap: () {
        setState(() {
          dataCat = Category.cate ;
        });

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => promoDetail()));
      },
    );

    Card makeCard(Category Category) => Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: makeListTile(Category),
      ),
    );

    final makeBody = Container(
      // decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, 1.0)),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: Categorys.length,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(Categorys[index]);
        },
      ),
    );




    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Majnnh Store',
            style: TextStyle(color: Colors.red),),
      ),
      backgroundColor: Colors.white,
      body: makeBody,
    );
  }
}

List getCategorys() {
  return [
    Category(
        title: "All",
        cate: "ALL",
        content:"assets/icons/basket.png"),

    Category(
        title: "Beverages",
        cate: "B",
        content:"assets/icons/beverages.png"),

    Category(
      content: "assets/icons/milk.png",
      cate: "DP",
      title: "Dairy Products",
    ),
    Category(
      content: "assets/icons/snacks.png",
      cate: "DS",
      title: "Desserts & Snacks ",
    ),
    Category(
      content: "assets/icons/cleaner.png",
      cate: "C",
      title: "Cleaners",

    ),
    Category(
      content: "assets/icons/kit.png",
      cate: "KG",
      title: "Kitchen Goods",


    ),

    Category(
      content: "assets/icons/grains.png",
      cate: "SG",
      title: "Spices & Grains",

    ),

    Category(
      content: "assets/icons/can.png",
      cate: "OC",
      title: "Oil & Canning",

    ),
    Category(
      content: "assets/icons/lotion.png",
      cate: "PC",
      title: "Personal Care",

    ),
    Category(
      content: "assets/icons/baby.png",
      cate: "BC",
      title: "Baby Care",

    ),
    Category(
      content: "assets/icons/bread.png",
      cate: "BBG",
      title: "Bakery & Baking Goods",

    ),
  ];
}
