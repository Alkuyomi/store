import 'package:flutter/material.dart';
import 'package:store_main_version/ListItem/CartItemData.dart';
import 'package:store_main_version/UI/CartUIComponent/Delivery.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:store_main_version/UI/BottomNavigationBar.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:store_main_version/ListItem/Data.dart' ;
import 'dart:convert';

class cart extends StatefulWidget {
  @override
  _cartState createState() => _cartState();
}
enum Answers{YES,CANCEL}

class _cartState extends State<cart> {

    double t = 0.0 ;


  @override
  void initState() {
    super.initState();
    print("START");
    print("T : ${t}");




    cartItems.forEach((e) => {
        t += e.price * e.quantity
    });


    print("T : ${t}");


  }

  /// Declare price and value for chart
  //int value = 1;

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: Color(0xFFf73f52)),
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              "Cart",
              style: TextStyle(
                  fontFamily: "Gotik",
                  fontSize: 18.0,
                  color: Colors.black54,
                  fontWeight: FontWeight.w700),
            ),
            elevation: 0.0,
          ),

          ///
          ///
          /// Checking item value of cart
          ///
          ///
          ///
          body: Column(

            children: <Widget>[

              Expanded(
                flex: 1,
                child: cartItems.length>0?
                ListView.builder(
                  itemCount: cartItems.length,
                  itemBuilder: (context,position){


                    ///
                    /// Widget for list view slide delete
                    ///
                    return Slidable(

                      delegate: new SlidableDrawerDelegate(),
                      actionExtentRatio: 0.25,

                      secondaryActions: <Widget>[
                        new IconSlideAction(
                          key: Key(cartItems[position].id.toString()),
                          caption: 'Delete',
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () {
                            var name = cartItems[position].name;
                            setState(() {
                              t -= cartItems[position].price;
                              cartItems.removeAt(position);

                            });
                            ///
                            /// SnackBar show if cart delet
                            ///
                            Scaffold.of(context)
                                .showSnackBar(SnackBar(content: Text("${name}  Deleted"),duration: Duration(seconds: 2),backgroundColor: Colors.redAccent,));
                          },
                        ),
                      ],
                      child: Padding(
                        padding: const EdgeInsets.only(top: 1.0, left: 13.0, right: 13.0),
                        /// Background Constructor for card
                        child: Container(
                          margin: const EdgeInsets.only(top: 10.0),
                          height: 220.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12.withOpacity(0.1),
                                blurRadius: 3.5,
                                spreadRadius: 0.4,
                              )
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              Row(

                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(10.0),

                                      /// Image item
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white.withOpacity(0.1),
//                                        boxShadow: [
//                                          BoxShadow(
//                                              color: Colors.black12.withOpacity(0.1),
//                                              blurRadius: 0.5,
//                                              spreadRadius: 0.1)
//                                        ]
                                          ),
                                          child: Image.network('${cartItems[position].img}',
                                            height: 130.0,
                                            width: 120.0,

                                          ))),
                                  Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 25.0, left: 10.0, right: 5.0),
                                      child: Column(

                                        /// Text Information Item
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            '${cartItems[position].name}',
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontFamily: "Sans",
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),

                                          Padding(padding: EdgeInsets.only(top: 10.0)),
                                          Text('${cartItems[position].price.toStringAsFixed(3)}'),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 18.0, left: 0.0),
                                            child: Container(
                                              width: 112.0,
                                              decoration: BoxDecoration(

                                                  color: Colors.white70,
                                                  border: Border.all(
                                                      color: Colors.black12.withOpacity(0.1)
                                                  ),


                                              ),

                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment
                                                    .spaceAround,
                                                children: <Widget>[

                                                  /// Decrease of value item
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        if(cartItems[position].quantity != 1){
                                                          cartItems[position].quantity-- ;
                                                          t -= cartItems[position].price ;
                                                        }

                                                      });
                                                    },
                                                    child: Container(
                                                      height: 30.0,
                                                      width: 30.0,
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              right: BorderSide(
                                                                  color: Colors.black12
                                                                      .withOpacity(0.1)))),
                                                      child: Center(child: Text("-")),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 10.0),
                                                    child: Text(cartItems[position].quantity.toString()),
                                                  ),

                                                  /// Increasing value of item
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        cartItems[position].quantity++ ;
                                                        t += cartItems[position].price ;
                                                        //value = value + 1;

                                                      });
                                                    },
                                                    child: Container(
                                                      height: 30.0,
                                                      width: 28.0,
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              left: BorderSide(
                                                                  color: Colors.black12
                                                                      .withOpacity(0.1)))),
                                                      child: Center(child: Text("+")),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.0)),
                              Divider(
                                height: 2.0,
                                color: Colors.black12,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 9.0, left: 10.0, right: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10.0 , top: 10.0),
                                      /// Total price of item buy
                                      child: Text(
                                        "Total :  " + '${(cartItems[position].price * cartItems[position].quantity).toStringAsFixed(3)} OMR' ,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14.5,
                                            fontFamily: "Sans"),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {

                                        Navigator.of(context).push(PageRouteBuilder(
                                            pageBuilder: (_, __, ___) => delivery()));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(right: 10.0),

                                      ),

                                    ),


                            ],

                                ),

                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  scrollDirection: Axis.vertical,
                ): noItemCart(),
              ),


//              Padding(
//                padding: const EdgeInsets.only(bottom: 0.0),
//                child: Container(
//
//
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//
//                      /// Button Pay
//                      InkWell(
//                        onTap: () {
//                          Navigator.of(context).push(PageRouteBuilder(
//                              pageBuilder: (_, __, ___) => new delivery()));
//                        },
//
//                        child: Container(
//                          height: 45.0,
//                          width: 200.0,
//
//                          decoration: BoxDecoration(
//                            color: Colors.indigoAccent,
//                            borderRadius: BorderRadius.all(Radius.circular(40.0)
//                            ),
//                          ),
//                          child: Center(
//                            child: Text(
//                              "Oreder     ${t.toStringAsFixed(3)} OMR",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontWeight: FontWeight.w700),
//                            ),
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
            ],
          ),

floatingActionButton:     FloatingActionButton.extended(
  backgroundColor: Colors.blue,
  label: Text("Oreder     ${t.toStringAsFixed(3)} OMR"),
  icon: Icon(Icons.shopping_basket),
  onPressed: () {



    Future _askUser() async {
      switch(
      await showDialog(
          context: context,
          /*it shows a popup with few options which you can select, for option we
        created enums which we can use with switch statement, in this first switch
        will wait for the user to select the option which it can use with switch cases*/
          child: new SimpleDialog(
            title: new Text('Complete oreder?'),
            children: <Widget>[
              new SimpleDialogOption(child: new Text('Yes '),onPressed: (){Navigator.pop(context, Answers.YES);},),
              new SimpleDialogOption(child: new Text('Cancel '),onPressed: (){Navigator.pop(context, Answers.CANCEL);},),
            ],
          )
      )
      ) {
        case Answers.YES:




          // Function to get the JSON data
          Future<String> sendOrder() async {

            var obj =  '{"total": "${t.toStringAsFixed(3)}" ,  ' ;
            cartItems.forEach((e) =>  obj +=  '"${e.id}":  {"name": "${e.name}" , "quantity": "${e.quantity}" , "price": "${e.price.toStringAsFixed(3)}" , "total": "${(e.price * e.quantity).toStringAsFixed(3)}"},' );
           // obj = obj.substring(0, obj.length-1);
            obj += ' "user": {"name": "${await getName()}" , "phone": "${await getPhone()}" ,  "location": "${await getLocation()}" ,  "email": "${await getEmail()}"}  }';

            var response = await http.post(
                Uri.encodeFull('https://us-central1-grocery-project-1a3b7.cloudfunctions.net/addOrder'),
                headers: {"Accept": "application/json"} ,
                body: obj);
            print(response);
            return "Successfull";
          }
          sendOrder();
          cartItems = [];
          Fluttertoast.showToast(
              msg: "Order sent",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0
          );
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => bottomNavigationBar()),
          );
          break;
        case Answers.CANCEL:
          Navigator.canPop(context);
          break;

      }
    }
    _askUser();

//    Navigator.of(context).push(PageRouteBuilder(
//     pageBuilder: (_, __, ___) => new delivery()));
  },

),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    }





  }

  ///
///
/// If no item cart this class showing
///
class noItemCart extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return  Container(
      width: 500.0,
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                EdgeInsets.only(top: mediaQueryData.padding.top + 50.0)),
            Image.network(
              "assets/imgIllustration/IlustrasiCart.png",
              height: 300.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 10.0)),
            Text(
              "No Items Added Yet!",
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 18.5,
                  color: Colors.black26.withOpacity(0.2),
                  fontFamily: "Popins"),
            ),
          ],
        ),
      ),
    );
  }
}
