import 'dart:async';

import 'package:flutter/material.dart';
import 'package:store_main_version/UI/BottomNavigationBar.dart';
import 'package:store_main_version/UI/LoginOrSignup/Login.dart';
import 'package:store_main_version/UI/LoginOrSignup/LoginAnimation.dart';
import 'package:store_main_version/UI/LoginOrSignup/Signup.dart';
import 'package:http/http.dart' as http;
import 'package:store_main_version/ListItem/Data.dart' ;

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}
var EMAIL , NAME , LOCATION ,PHONE ;

class _SignupState extends State<Signup> with TickerProviderStateMixin {
  //Animation Declaration
  AnimationController sanimationController;
  AnimationController animationControllerScreen;
  Animation animationScreen;
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();

  var tap = 0;

  /// Set AnimationController to initState
  @override
  void initState() {

    sanimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800))
          ..addStatusListener((statuss) {
            if (statuss == AnimationStatus.dismissed) {
              setState(() {
                tap = 0;
              });
            }
          });
    // TODO: implement initState
    super.initState();
  }

  /// Dispose animationController
  @override
  void dispose() {
    super.dispose();
    sanimationController.dispose();
  }

  /// Playanimation set forward reverse
  Future<Null> _PlayAnimation() async {
    try {
      await sanimationController.forward();
      await sanimationController.reverse();
    } on TickerCanceled {}
  }
  /// Component Widget layout UI
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    mediaQueryData.devicePixelRatio;
    mediaQueryData.size.height;
    mediaQueryData.size.width;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
//            /// Set Background image in layout
//            decoration: BoxDecoration(
//                image: DecorationImage(
//              image: AssetImage("assets/img/backgroundgirl.png"),
//              fit: BoxFit.cover,
//            )),
            child: Container(
              /// Set gradient color in image
              decoration: BoxDecoration(
//                gradient: LinearGradient(
//                  colors: [
//                    Color.fromRGBO(0, 0, 0, 0.2),
//                    Color.fromRGBO(0, 0, 0, 0.3)
//                  ],
//                  begin: FractionalOffset.topCenter,
//                  end: FractionalOffset.bottomCenter,
//                ),
              color: Colors.white
              ),
              /// Set component layout
              child: ListView(
                padding: EdgeInsets.all(0.0),
                children: <Widget>[
                  Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            alignment: AlignmentDirectional.topCenter,
                            child: Column(
                              children: <Widget>[
                                /// padding logo
                                Padding(
                                    padding: EdgeInsets.only(
                                        top:
                                            mediaQueryData.padding.top + 25.0)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage("assets/icons/brand.png"),
                                      height: 200.0,
                                    ),

                                    /// Animation text Grocery shop accept from login layout

                                  ],
                                ),
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 1.0)),
                                /// TextFromField Email
                               textFromField(tff:  TextFormField(
                                 controller: nameController,

                                 decoration: InputDecoration(
                                     border: InputBorder.none,
                                     labelText: 'name',
                                     icon: Icon(
                                       Icons.person,
                                       color: Colors.black38,
                                     ),
                                     labelStyle: TextStyle(
                                         fontSize: 15.0,
                                         fontFamily: 'Sans',
                                         letterSpacing: 0.3,
                                         color: Colors.black38,
                                         fontWeight: FontWeight.w600)),
                               ),
),
                                Padding(
                                    padding:
                                    EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                textFromField(tff:  TextFormField(
                                  controller: emailController,

                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      labelText: 'email',
                                      icon: Icon(
                                        Icons.email,
                                        color: Colors.black38,
                                      ),
                                      labelStyle: TextStyle(
                                          fontSize: 15.0,
                                          fontFamily: 'Sans',
                                          letterSpacing: 0.3,
                                          color: Colors.black38,
                                          fontWeight: FontWeight.w600)),
                                ),
                                ),
                                Padding(
                                    padding:
                                    EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                textFromField(tff:  TextFormField(
                                  controller: locationController,

                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      labelText: 'Location',
                                      icon: Icon(
                                        Icons.location_on,
                                        color: Colors.black38,
                                      ),
                                      labelStyle: TextStyle(
                                          fontSize: 15.0,
                                          fontFamily: 'Sans',
                                          letterSpacing: 0.3,
                                          color: Colors.black38,
                                          fontWeight: FontWeight.w600)),
                                ),
                                ),

                                Padding(
                                    padding:
                                    EdgeInsets.symmetric(vertical: 5.0)),

                                /// TextFromField Password
                                textFromField(tff:  TextFormField(
                                  controller: phoneController,

                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      labelText: 'Phone Number',
                                      icon: Icon(
                                        Icons.phone_android,
                                        color: Colors.black38,
                                      ),
                                      labelStyle: TextStyle(
                                          fontSize: 15.0,
                                          fontFamily: 'Sans',
                                          letterSpacing: 0.3,
                                          color: Colors.black38,
                                          fontWeight: FontWeight.w600)),
                                ),
                                ),


                                Padding(
                                  padding: EdgeInsets.only(
                                      top: mediaQueryData.padding.top + 100.0,
                                      bottom: 0.0),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),

                      /// Set Animaion after user click buttonLogin
                      tap == 0
                          ? InkWell(
                              splashColor: Colors.yellow,
                              onTap: () {
                                var obj = '{"email": "${emailController.text}" , "phone": "${phoneController.text}" ,"location": "${locationController.text}" ,"name": "${nameController.text}" }' ;



                                // Function to get the JSON data
                                Future<String> sendUser() async {
                                  var response = await http.post(
                                      Uri.encodeFull('https://us-central1-grocery-project-1a3b7.cloudfunctions.net/addUser'),
                                      headers: {"Accept": "application/json"} ,
                                      body: obj);
                                  await setEmail(emailController.text);
                                  await setPhone(phoneController.text);
                                  await setName(nameController.text);
                                  await setLocation(locationController.text);
                                  print(await getName());
                                  return "Successfull";
                                }
                                sendUser();

                               print(emailController.text);


                                setState(() {
                                  tap = 1;
                                });
                                _PlayAnimation();
                                return tap;
                              },
                              child: buttonBlackBottom(),
                            )
                          : new LoginAnimation(
                              animationController: sanimationController.view,
                            )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// textfromfield custom class
class textFromField extends StatelessWidget {
  bool password;
  String email;
  IconData icon;
  TextInputType inputType;
  String type ;
  TextFormField tff ;

  textFromField({this.email, this.icon, this.inputType, this.password , this.type, this.tff});


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      child: Container(
        height: 60.0,
        alignment: AlignmentDirectional.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14.0),
            color: Colors.white,
            boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        padding:
            EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
        child: Theme(
          data: ThemeData(
            hintColor: Colors.transparent,
          ),
          child: tff != null ? tff :  TextField(
            obscureText: password,
            decoration: InputDecoration(
                border: InputBorder.none,
                labelText: email,
                icon: Icon(
                  icon,
                  color: Colors.black38,
                ),
                labelStyle: TextStyle(
                    fontSize: 15.0,
                    fontFamily: 'Sans',
                    letterSpacing: 0.3,
                    color: Colors.black38,
                    fontWeight: FontWeight.w600)),
            keyboardType: inputType,


          ),
        ),
      ),
    );
  }
}

///ButtonBlack class
class buttonBlackBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30.0),
      child: Container(
        height: 55.0,
        width: 600.0,
        child: Text(
          "Start shopping",
          style: TextStyle(
              color: Colors.white,
              letterSpacing: 0.2,
              fontFamily: "Sans",
              fontSize: 18.0,
              fontWeight: FontWeight.w800),
        ),
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 15.0)],
            borderRadius: BorderRadius.circular(30.0),
            gradient: LinearGradient(
                colors: <Color>[Color(0xFFCB356B), Color(0xFFf73f52)])),
      ),
    );
  }
}
